<?php

/**
 * @file classes/plugins/GenericPlugin.inc.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class GenericPlugin
 * @ingroup plugins
 *
 * @brief Abstract class for generic plugins
 */

// $Id: GenericPlugin.inc.php,v 1.1.1.1 2008/10/20 21:27:08 tylerl Exp $


import('plugins.Plugin');

class GenericPlugin extends Plugin {
	// No additional functions for now.
}

?>
