<?php

/**
 * @file classes/monographMonographFileDAO.inc.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class MonographFileDAO
 * @ingroup monograph
 * @see MonographFile
 *
 * @brief Operations for retrieving and modifying MonographFile objects.
 */

// $Id: MonographFileDAO.inc.php,v 1.12 2009/06/24 00:03:05 tylerl Exp $

import('monograph.MonographArtworkFile');
import('file.MonographFileManager');

define('INLINEABLE_TYPES_FILE', Config::getVar('general', 'registry_dir') . DIRECTORY_SEPARATOR . 'inlineTypes.txt');

class MonographFileDAO extends DAO {
	/**
	 * Array of MIME types that can be displayed inline in a browser
	 */
	var $inlineableTypes;

	/**
	 * Retrieve a monograph file by ID.
	 * @param $fileId int
	 * @param $revision int optional, if omitted latest revision is used
	 * @param $monographId int optional
	 * @return MonographFile
	 */
	function &getMonographFile($fileId, $revision = null, $monographId = null) {
		if ($fileId === null) {
			$returner = null;
			return $returner;
		}
		if ($revision == null) {
			if ($monographId != null) {
				$result =& $this->retrieveLimit(
					'SELECT a.* FROM monograph_files a WHERE file_id = ? AND monograph_id = ? ORDER BY revision DESC',
					array($fileId, $monographId),
					1
				);
			} else {
				$result =& $this->retrieveLimit(
					'SELECT a.* FROM monograph_files a WHERE file_id = ? ORDER BY revision DESC',
					$fileId,
					1
				);
			}

		} else {
			if ($monographId != null) {
				$result =& $this->retrieve(
					'SELECT a.* FROM monograph_files a WHERE file_id = ? AND revision = ? AND monograph_id = ?',
					array($fileId, $revision, $monographId)
				);
			} else {
				$result =& $this->retrieve(
					'SELECT a.* FROM monograph_files a WHERE file_id = ? AND revision = ?',
					array($fileId, $revision)
				);
			}
		}

		$returner = null;
		if (isset($result) && $result->RecordCount() != 0) {
			$returner =& $this->_fromRow($result->GetRowAssoc(false));
		}

		$result->Close();
		unset($result);

		return $returner;
	}
  
	/**
	 * Retrieve all revisions of a monograph file.
	 * @param $monographId int
	 * @return MonographFile
	 */
	function &getMonographFileRevisions($fileId, $reviewType = null, $round = null) {
		if ($fileId === null) {
			$returner = null;
			return $returner;
		}
		$monographFiles = array();

		if ($reviewType == null) {
			$result =& $this->retrieve(
				'SELECT a.* FROM monograph_files a WHERE file_id = ? ORDER BY revision',
				$fileId
			);
		} elseif ( $round == null ) {
			$result =& $this->retrieve(
				'SELECT a.* FROM monograph_files a WHERE file_id = ? AND review_type = ? ORDER BY revision',
				array($fileId, $reviewType)
			);
		} else {
			$result =& $this->retrieve(
				'SELECT a.* FROM monograph_files a WHERE file_id = ? AND review_type = ? AND round = ? ORDER BY revision',
				array($fileId, $reviewType, $round)
			);
		}

		while (!$result->EOF) {
			$monographFiles[] =& $this->_fromRow($result->GetRowAssoc(false));
			$result->moveNext();
		}

		$result->Close();
		unset($result);

		return $monographFiles;
	}

	/**
	 * Retrieve revisions of a monograph file in a range.
	 * @param $monographId int
	 * @return MonographFile
	 */
	function &getMonographFileRevisionsInRange($fileId, $start = 1, $end = null) {
		if ($fileId === null) {
			$returner = null;
			return $returner;
		}
		$monographFiles = array();

		if ($end == null) {
			$result =& $this->retrieve(
				'SELECT a.* FROM monograph_files a WHERE file_id = ? AND revision >= ?',
				array($fileId, $start)
			);
		} else {
			$result =& $this->retrieve(
				'SELECT a.* FROM monograph_files a WHERE file_id = ? AND revision >= ? AND revision <= ?',
				array($fileId, $start, $end)
			);		
		}

		while (!$result->EOF) {
			$monographFiles[] =& $this->_fromRow($result->GetRowAssoc(false));
			$result->moveNext();
		}

		$result->Close();
		unset($result);

		return $monographFiles;
	}

	/**
	 * Retrieve the current revision number for a file.
	 * @param $fileId int
	 * @return int
	 */
	function &getRevisionNumber($fileId) {
		if ($fileId === null) {
			$returner = null;
			return $returner;
		}
		$result =& $this->retrieve(
			'SELECT MAX(revision) AS max_revision FROM monograph_files a WHERE file_id = ?',
			$fileId
		);

		if ($result->RecordCount() == 0) {
			$returner = null;
		} else {
			$row = $result->FetchRow();
			$returner = $row['max_revision'];
		}

		$result->Close();
		unset($result);

		return $returner;
	}

	/**
	 * Retrieve all monograph files for a monograph.
	 * @param $monographId int
	 * @return array MonographFiles
	 */
	function &getByMonographId($monographId, $type = null) {
		$monographFiles = array();

		$sqlParams = array($monographId);
		$sqlExtra = '';

		if (isset($type)) {
			$sqlExtra .= ' AND type = ? ';
			$sqlParams[] = $type;
		}

		$result =& $this->retrieve(
			'SELECT * FROM monograph_files
			WHERE monograph_id = ?'.$sqlExtra, $sqlParams
		);

		while (!$result->EOF) {
			$monographFiles[] =& $this->_fromRow($result->GetRowAssoc(false));
			$result->moveNext();
		}

		$result->Close();
		unset($result);

		return $monographFiles;
	}

	/**
	 * Retrieve all monograph files for a type and assoc ID.
	 * @param $assocId int
	 * @param $type int
	 * @param $monographId int
	 * @return array MonographFiles
	 */
	function &getMonographFilesByAssocId($assocId, $type, $monographId) {

		$locale = Locale::getLocale();
		$primaryLocale = Locale::getPrimaryLocale();

		$monographFiles = array();

		$result =& $this->retrieve(
			'SELECT mf.*, mfs.*,
				COALESCE(mcs.setting_value, mcs0.setting_value) AS component_title
			FROM monograph_files mf
			LEFT JOIN monograph_file_settings mfs ON mf.file_id = mfs.file_id AND mfs.setting_name = \'componentId\'
			LEFT JOIN monograph_components mc ON mfs.setting_value = mc.component_id
			LEFT JOIN monograph_component_settings mcs ON (mcs.component_id = mc.component_id AND mcs.setting_name = ? AND mcs.locale = ?)
			LEFT JOIN monograph_component_settings mcs0 ON (mcs0.component_id = mc.component_id AND mcs0.setting_name = ? AND mcs0.locale = ?)
			WHERE mf.type = ? AND mf.monograph_id = ?',
			array('title', $primaryLocale, 'title', $locale, MonographFileManager::typeToPath($type), $monographId)
		);

		while (!$result->EOF) {
			$monographFiles[] =& $this->_fromRow($result->GetRowAssoc(false));
			$result->moveNext();
		}

		$result->Close();
		unset($result);

		return $monographFiles;
	}

	/**
	 * Construct a new data object corresponding to this DAO.
	 * @return SignoffEntry
	 */
	function newDataObject() {
		return new MonographFile();
	}

	/**
	 * Internal function to return a MonographFile object from a row.
	 * @param $row array
	 * @return MonographFile
	 */
	function &_fromRow(&$row) {
		$monographFile = $this->newDataObject();

		$monographFile->setFileId($row['file_id']);
		$monographFile->setSourceFileId($row['source_file_id']);
		$monographFile->setSourceRevision($row['source_revision']);
		$monographFile->setRevision($row['revision']);
		$monographFile->setMonographId($row['monograph_id']);
		$monographFile->setFileName($row['file_name']);
		$monographFile->setFileType($row['file_type']);
		$monographFile->setFileSize($row['file_size']);
		$monographFile->setOriginalFileName($row['original_file_name']);
		$monographFile->setType($row['type']);
		$monographFile->setLocaleKeyForType(MonographFileManager::pathToLocaleKey($row['type']));
		$monographFile->setAssocId($row['assoc_id']);
		$monographFile->setDateUploaded($this->datetimeFromDB($row['date_uploaded']));
		$monographFile->setDateModified($this->datetimeFromDB($row['date_modified']));
		$monographFile->setRound($row['round']);
		$monographFile->setViewable($row['viewable']);
		$monographFile->setReviewType($row['review_type']);
		$monographFile->setSortableByComponent($row['sortable_by_component']);

		$monographFileSettingsDao =& DAORegistry::getDAO('MonographFileSettingsDAO');
		$monographFileSettings =& $monographFileSettingsDao->getSettingsByFileId($monographFile->getFileId());
		$monographFile->setSettings($monographFileSettings);

		HookRegistry::call('MonographFileDAO::_fromRow', array(&$monographFile, &$row));

		return $monographFile;
	}

	/**
	 * Insert a new MonographFile.
	 * @param $monographFile MonographFile
	 * @return int
	 */	
	function insertMonographFile(&$monographFile) {
		$fileId = $monographFile->getFileId();
		$params = array(
			$monographFile->getRevision() === null ? 1 : $monographFile->getRevision(),
			$monographFile->getMonographId(),
			$monographFile->getSourceFileId(),
			$monographFile->getSourceRevision(),
			$monographFile->getFileName(),
			$monographFile->getFileType(),
			$monographFile->getFileSize(),
			$monographFile->getOriginalFileName(),
			$monographFile->getType(),
			$monographFile->getViewable(),
			$monographFile->getAssocId(),
			$monographFile->getReviewType(),
			$monographFile->getRound(),
			$monographFile->getSortableByComponent()
		);

		if ($fileId) {
			array_unshift($params, $fileId);
		}

		$this->update(
			sprintf('INSERT INTO monograph_files
				(' . ($fileId ? 'file_id, ' : '') . 'revision, monograph_id, source_file_id, source_revision, file_name, file_type, file_size, original_file_name, type, date_uploaded, date_modified, viewable, assoc_id, review_type, round, sortable_by_component)
				VALUES
				(' . ($fileId ? '?, ' : '') . '?, ?, ?, ?, ?, ?, ?, ?, ?, %s, %s, ?, ?, ?, ?, ?)',
				$this->datetimeToDB($monographFile->getDateUploaded()), $this->datetimeToDB($monographFile->getDateModified())),
			$params
		);

		if (!$fileId) {
			$monographFile->setFileId($this->getInsertMonographFileId());
		}

		return $monographFile->getFileId();
	}

	/**
	 * Update an existing monograph file.
	 * @param $monograph MonographFile
	 */
	function updateMonographFile(&$monographFile) {
		$this->update(
			sprintf('UPDATE monograph_files
				SET
					monograph_id = ?,
					source_file_id = ?,
					source_revision = ?,
					file_name = ?,
					file_type = ?,
					file_size = ?,
					original_file_name = ?,
					type = ?,
					date_uploaded = %s,
					date_modified = %s,
					round = ?,
					review_type = ?,
					viewable = ?,
					assoc_id = ?,
					sortable_by_component = ?
				WHERE file_id = ? AND revision = ?',
				$this->datetimeToDB($monographFile->getDateUploaded()), $this->datetimeToDB($monographFile->getDateModified())),
			array(
				$monographFile->getMonographId(),
				$monographFile->getSourceFileId(),
				$monographFile->getSourceRevision(),
				$monographFile->getFileName(),
				$monographFile->getFileType(),
				$monographFile->getFileSize(),
				$monographFile->getOriginalFileName(),
				$monographFile->getType(),
				$monographFile->getRound() == null ? 1 : $monographFile->getRound(),//temporary
				$monographFile->getReviewType(),
				$monographFile->getViewable(),
				$monographFile->getAssocId(),
				$monographFile->getSortableByComponent(),
				$monographFile->getFileId(),
				$monographFile->getRevision()
			)
		);

		return $monographFile->getFileId();

	}

	/**
	 * Delete a monograph file.
	 * @param $monograph MonographFile
	 */
	function deleteMonographFile(&$monographFile) {
		return $this->deleteMonographFileById($monographFile->getFileId(), $monographFile->getRevision());
	}

	/**
	 * Delete a monograph file by ID.
	 * @param $monographId int
	 * @param $revision int
	 */
	function deleteMonographFileById($fileId, $revision = null) {
		$monographFileSettingsDao =& DAORegistry::getDAO('MonographFileSettingsDAO');

		if ($revision == null) {
			$this->update(
				'DELETE FROM monograph_files WHERE file_id = ?', $fileId
			);
		} else {
			$this->update(
				'DELETE FROM monograph_files WHERE file_id = ? AND revision = ?', array($fileId, $revision)
			);
		}

		$monographFileSettingsDao->deleteSettingsByFileId($fileId, $revision);
	}

	/**
	 * Delete all monograph files for a monograph.
	 * @param $monographId int
	 */
	function deleteMonographFiles($monographId) {
		return $this->update(
			'DELETE FROM monograph_files WHERE monograph_id = ?', $monographId
		);
	}

	/**
	 * Get the ID of the last inserted monograph file.
	 * @return int
	 */
	function getInsertMonographFileId() {
		return $this->getInsertId('monograph_files', 'file_id');
	}

	/**
	 * Check whether a file may be displayed inline.
	 * @param $monographFile object
	 * @return boolean
	 */
	function isInlineable(&$monographFile) {
		if (!isset($this->inlineableTypes)) {
			$this->inlineableTypes = array_filter(file(INLINEABLE_TYPES_FILE), create_function('&$a', 'return ($a = trim($a)) && !empty($a) && $a[0] != \'#\';'));
		}
		return in_array($monographFile->getFileType(), $this->inlineableTypes);
	}
}

?>