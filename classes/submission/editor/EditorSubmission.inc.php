<?php

/**
 * @file classes/submission/AcquisitionsEditor/AcquisitionsEditorSubmission.inc.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class AcquisitionsEditorSubmission
 * @ingroup submission
 * @see AcquisitionsEditorSubmissionDAO
 *
 * @brief AcquisitionsEditorSubmission class.
 */

// $Id: EditorSubmission.inc.php,v 1.8 2009/06/23 00:56:37 jalperin Exp $


import('submission.acquisitionsEditor.AcquisitionsEditorSubmission');

class EditorSubmission extends AcquisitionsEditorSubmission {

	/**
	 * Constructor.
	 */
	function EditorSubmission() { 
		parent::AcquisitionsEditorSubmission();
	}
}
?>
