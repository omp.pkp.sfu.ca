<?php

/**
 * @file classes/manager/form/SponsorForm.inc.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class SponsorForm
 * @ingroup manager_form_setup
 *
 * @brief Form for adding/edditing a sponsor
 */

import('form.Form');

class SponsorForm extends Form {
	/**
	 * Constructor.
	 * @param $emailKey string
	 */
	function SponsorForm() {
		parent::Form('manager/setup/form/sponsors.tpl');

		// Validation checks for this form
		$this->addCheck(new FormValidator($this, 'sponsors-institution', 'required', 'manager.setup.form.sponsors.institutionRequired'));
		$this->addCheck(new FormValidator($this, 'sponsors-url', 'required', 'manager.emails.form.sponsors.urlRequired'));
		$this->addCheck(new FormValidatorPost($this));
	}

	/**
	 * Display the form.
	 */
	function display() {
		parent::display();
	}

	/**
	 * Initialize form data from current settings.
	 */
	function initData() {
		$press =& Request::getPress();
		
		$this->_data = array(
			'sponsors-institution' => '',
			'sponsors-url' => ''
		);
	}

	/**
	 * Assign form data to user-submitted data.
	 */
	function readInputData() {
		$this->readUserVars(array('sponsors-institution', 'sponsors-url', 'sponsors-note'));
	}

	/**
	 * Save email template.
	 */
	function execute() {
		$press =& Request::getPress();
		//FIXME: need to localize this form properly
		$locale = Locale::getLocale();
		
		$sponsors = $press->getSetting('sponsors', $locale);
		
		$sponsors[$locale][] = array('institution' => $this->getData('sponsors-institution'), 
							'url' => $this->getData('sponsors-url'));
							
		$press->updateSetting('sponsors', $sponsors, 'object', true);
	}
}

?>
