<?php

/**
 * @file classes/user/UserDAO.inc.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class UserDAO
 * @ingroup user
 * @see PKPUserDAO
 *
 * @brief Basic class describing users existing in the system.
 */

// $Id: UserDAO.inc.php,v 1.1.1.1 2008/10/20 21:27:08 tylerl Exp $


import('user.User');
import('user.PKPUserDAO');

class UserDAO extends PKPUserDAO {


}

?>
