<?php

/**
 * @defgroup announcement
 */

/**
 * @file classes/announcement/Announcement.inc.php
 *
 * Copyright (c) 2003-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class Announcement
 * @ingroup announcement
 * @see AnnouncementDAO
 *
 * @brief Basic class describing a announcement.
 *
 */

// $Id: Announcement.inc.php,v 1.1 2009/06/26 16:44:53 jalperin Exp $

import('announcement.PKPAnnouncement');

class Announcement extends PKPAnnouncement {

}

?>
