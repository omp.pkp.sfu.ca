<?php

/**
 * @file classes/announcement/AnnouncementTypeDAO.inc.php
 *
 * Copyright (c) 2003-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class AnnouncementTypeDAO
 * @ingroup announcement
 * @see AnnouncementType
 *
 * @brief Operations for retrieving and modifying AnnouncementType objects.
 */

// $Id: AnnouncementTypeDAO.inc.php,v 1.1 2009/06/26 16:44:53 jalperin Exp $

import('announcement.AnnouncementType');
import('announcement.PKPAnnouncementTypeDAO');

class AnnouncementTypeDAO extends PKPAnnouncementTypeDAO {

}

?>