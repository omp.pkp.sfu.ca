<?php

/**
 * @file classes/announcement/AnnouncementType.inc.php
 *
 * Copyright (c) 2003-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class AnnouncementType
 * @ingroup announcement
 * @see AnnouncementTypeDAO, AnnouncementTypeForm
 *
 * @brief Basic class describing an announcement type.
 */

// $Id: AnnouncementType.inc.php,v 1.1 2009/06/26 16:44:53 jalperin Exp $

import('announcement.PKPAnnouncementType');

class AnnouncementType extends PKPAnnouncementType {

}

?>
