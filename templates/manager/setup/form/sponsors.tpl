{**
 * sponsors.tpl
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Sponsors grid form
 *
 * $Id$
 *}
<form name="editSponsorForm" id="editSponsorForm" method="post" action="{url op='updateSponsor'}">
{include file="common/formErrors.tpl"}

<h3>1.5 {translate key="manager.setup.sponsors"}</h3>

<p>{translate key="manager.setup.sponsorsDescription"}</p>

<table width="100%" class="data">
	<tr valign="top">
		<td width="20%" class="label">{fieldLabel name="sponsors-institution" key="manager.setup.institution"}</td>
		<td width="80%" class="value"><input type="text" name="sponsors-institution" id="sponsors-institution" size="40" maxlength="90" class="textField" /></td>
	</tr>
	<tr valign="top">
		<td width="20%" class="label">{fieldLabel name="sponsors-url" key="common.url"}</td>
		<td width="80%" class="value"><input type="text" name="sponsors-url" id="sponsors-url" size="40" maxlength="255" class="textField" /></td>
	</tr>
</table>

<p><input type="submit" name="addSponsor" value="{translate key="manager.setup.addSponsor"}" class="button" /></p>
<p><span class="formRequired">{translate key="common.requiredField"}</span></p>
</form>