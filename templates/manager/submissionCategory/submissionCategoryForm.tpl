{**
 * submissionCategoryForm.tpl
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form to create/modify a press category.
 *
 * $Id: submissionCategoryForm.tpl,v 1.4 2009/07/30 23:16:15 tylerl Exp $
 *}

<form name="category" id="category" method="get" action="{url op='updateSubmissionCategory'}">
<!-- Locale selector -->
{if count($formLocales) > 1}
<table width="100%" class="data">
	<tr align="right" valign="top">
		<td width="20%" class="label">{fieldLabel name="formLocale" key="form.formLanguage"}</td>
		<td width="80%" class="value">{modal_language_chooser}</td>
	</tr>
</table>
<div class="separator"></div>
{/if}

<!-- Form Errors -->
<p>
	<div id="formErrors">
		<ul class="formErrorList"></ul>
	</div>
</p>
	
<!-- Hidden form inputs -->	
<input type="hidden" id="static" name="editorAction" value="" />
<input type="hidden" id="static" name="userId" value="" />
<input type="hidden" id="static" name="arrangementType" value="{$smarty.const.CATEGORY_ARRANGEMENT}" />

<!-- The form -->
<table class="data" width="100%">
<tr valign="top">
	<td width="20%" class="label">{fieldLabel name="title" required="true" key="submissionCategory.title"}</td>
	<td width="80%" class="value">{form_locale_iterator}<input type="text" name="title[{$formLocale|escape}]" value="{$title[$formLocale]|escape}" id="title" size="40" maxlength="120" class="textField rule_required" />{/form_locale_iterator}</td>
</tr>
<tr valign="top">
	<td class="label">{fieldLabel name="abbrev" required="true" key="arrangement.abbreviation"}</td>
	<td class="value">{form_locale_iterator}<input type="text" name="abbrev[{$formLocale|escape}]" id="abbrev" value="{$abbrev[$formLocale]|escape}" size="20" maxlength="20" class="textField rule_required" />{/form_locale_iterator}&nbsp;&nbsp;{translate key="submissionCategory.abbreviation.example"}</td>
</tr>
<tr valign="top">
	<td class="label">{fieldLabel name="policy" key="manager.categories.policy"}</td>
	<td class="value">{form_locale_iterator}<textarea name="policy[{$formLocale|escape}]" rows="4" cols="40" id="policy" class="textArea">{$policy[$formLocale]|escape}</textarea>{/form_locale_iterator}</td>
</tr>
<tr valign="top">
	<td class="label">{fieldLabel name="reviewFormId" key="submission.reviewForm"}</td>
	<td class="value">
		<select name="reviewFormId" size="1" id="reviewFormId" class="selectMenu">
			<option value="">{translate key="manager.reviewForms.noneChosen"}</option>
			{html_options options=$reviewFormOptions selected=$reviewFormId}
		</select>
	</td>
</tr>
<tr valign="top">
	<td class="label">{fieldLabel suppressId="true" key="submission.indexing"}</td>
	<td class="value">
		<input type="checkbox" name="metaIndexed" id="metaIndexed" value="1" {if $metaIndexed}checked="checked"{/if} />
		{fieldLabel name="metaIndexed" key="manager.arrangement.submissionIndexing"}
	</td>
</tr>
<tr valign="top">
	<td class="label">{fieldLabel suppressId="true" key="submission.restrictions"}</td>
	<td class="value">
		<input type="checkbox" name="editorRestriction" id="editorRestriction" value="1" {if $editorRestriction}checked="checked"{/if} />
		{fieldLabel name="editorRestriction" key="manager.arrangement.editorRestriction"}
	</td>
</tr>
<tr valign="top">
	<td class="label">{fieldLabel name="hideAbout" key="navigation.about"}</td>
	<td class="value">
		<input type="checkbox" name="hideAbout" id="hideAbout" value="1" {if $hideAbout}checked="checked"{/if} />
		{fieldLabel name="hideAbout" key="manager.series.hideAbout"}
	</td>
</tr>
{if $commentsEnabled}
<tr valign="top">
	<td class="label">{fieldLabel name="disableComments" key="comments.readerComments"}</td>
	<td class="value">
		<input type="checkbox" name="disableComments" id="disableComments" value="1" {if $disableComments}checked="checked"{/if} />
		{fieldLabel name="disableComments" key="manager.series.disableComments"}
	</td>
</tr>
{/if}
</table>
</form>
