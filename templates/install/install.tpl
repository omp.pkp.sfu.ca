{**
 * install.tpl
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Installation form.
 *
 * $Id: install.tpl,v 1.2 2008/12/04 01:28:55 tylerl Exp $
 *}
{strip}
{assign var="pageTitle" value="installer.ompInstallation"}
{include file="core:install/install.tpl"}
{/strip}