{**
 * site.tpl
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * About the Press site.
 *
 * $Id: site.tpl,v 1.4 2009/03/09 22:43:20 tylerl Exp $
 *}
{strip}
{assign var="pageTitle" value="about.aboutSite"}
{include file="common/header.tpl"}
{/strip}

{if !empty($about)}
	<p>{$about|nl2br}</p>
{/if}

<h3>{translate key="press.presses"}</h3>
<ul class="plain">
{iterate from=presses item=press}
	<li>&#187; <a href="{url press=`$press->getPath()` page="about" op="index"}">{$press->getLocalizedName()|escape}</a></li>
{/iterate}
</ul>

<a href="{url op="aboutThisPublishingSystem"}">{translate key="about.aboutThisPublishingSystem"}</a>

{include file="common/footer.tpl"}
