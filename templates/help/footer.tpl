{**
 * footer.tpl
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common footer for help pages.
 *
 * $Id: footer.tpl,v 1.1.1.1 2008/10/20 21:27:09 tylerl Exp $
 *}
{call_hook name="Templates::Help::Footer::PageFooter"}
</div>
</div>
</body>
</html>
