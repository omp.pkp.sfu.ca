{**
 * information.tpl
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Press information page.
 *
 * $Id: information.tpl,v 1.1.1.1 2008/10/20 21:27:09 tylerl Exp $
 *}
{strip}
{include file="common/header.tpl"}
{/strip}

<p>{$content|nl2br}</p>

{include file="common/footer.tpl"}
