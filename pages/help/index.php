<?php

/**
 * @defgroup pages_help
 */
 
/**
 * @file pages/help/index.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup pages_help
 * @brief Handle requests for viewing help pages. 
 *
 */

// $Id: index.php,v 1.1.1.1 2008/10/20 21:27:08 tylerl Exp $


define('HANDLER_CLASS', 'HelpHandler');

import('pages.help.HelpHandler');

?>
