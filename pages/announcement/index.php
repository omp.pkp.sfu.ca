<?php

/**
 * @defgroup pages_announcement
 */
 
/**
 * @file pages/announcement/index.php
 *
 * Copyright (c) 2003-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Handle requests for public announcement functions. 
 *
 * @ingroup pages_announcement
 * @brief Handle requests for public announcement functions. 
 *
 */

// $Id: index.php,v 1.1 2009/06/26 16:44:53 jalperin Exp $


define('HANDLER_CLASS', 'AnnouncementHandler');

import('pages.announcement.AnnouncementHandler');

?>
