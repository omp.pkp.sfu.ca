<?php

/**
 * @defgroup pages_production
 */
 
/**
 * @file pages/production/index.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup pages_production
 * @brief Handle information requests. 
 *
 */

// $Id: index.php,v 1.2 2009/05/07 06:35:04 tylerl Exp $


switch ($op) {

default:
	define('HANDLER_CLASS', 'ProductionEditorHandler');
	import('pages.productionEditor.ProductionEditorHandler');
	break;
}
?>
