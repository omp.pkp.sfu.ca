<?xml version="1.0" encoding="UTF-8"?>

<!--
  * pressSettings.xml
  *
  * Copyright (c) 2003-2008 John Willinsky
  * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
  *
  * Default press settings.
  *
  * $Id: pressSettings.xml,v 1.6 2009/09/09 21:53:57 tylerl Exp $
  -->

<!DOCTYPE press_settings [
	<!ELEMENT press_settings (setting+)>
	<!ELEMENT setting (name, value)>
	<!ATTLIST setting type (int|string|object|bool) #REQUIRED>
	<!ATTLIST setting locale (0|1) #REQUIRED>
	<!ELEMENT name (#PCDATA)>
	<!ELEMENT value (#PCDATA | array)*>
	<!ELEMENT element (#PCDATA | array)*>
	<!ATTLIST element key CDATA #IMPLIED>
	<!ELEMENT array (element+)>
]>

<press_settings>
	<setting type="int" locale="0">
		<name>numPageLinks</name>
		<value>10</value>
	</setting>
	<setting type="int" locale="0">
		<name>itemsPerPage</name>
		<value>25</value>
	</setting>
	<setting type="int" locale="0">
		<name>numWeeksPerReview</name>
		<value>4</value>
	</setting>
	<setting type="string" locale="1">
		<name>privacyStatement</name>
		<value>{translate key="default.pressSettings.privacyStatement"}</value>
	</setting>
	<setting type="string" locale="1">
		<name>openAccessPolicy</name>
		<value>{translate key="default.pressSettings.openAccessPolicy"}</value>
	</setting>
	<setting type="string" locale="1">
		<name>copyeditInstructions</name>
		<value>{translate key="default.pressSettings.copyeditInstructions"}</value>
	</setting>
	<setting type="string" locale="0">
		<name>emailSignature</name>
		<value>{translate key="default.pressSettings.emailSignature"}</value>
	</setting>
	<setting type="string" locale="1">
		<name>proofInstructions</name>
		<value>{translate key="default.pressSettings.proofingInstructions"}</value>
	</setting>
	<setting type="string" locale="1">
		<name>refLinkInstructions</name>
		<value>{translate key="default.pressSettings.refLinkInstructions"}</value>
	</setting>
	<setting type="string" locale="1">
		<name>readerInformation</name>
		<value>{translate key="default.pressSettings.forReaders"}</value>
	</setting>
	<setting type="string" locale="1">
		<name>authorInformation</name>
		<value>{translate key="default.pressSettings.forAuthors"}</value>
	</setting>
	<setting type="string" locale="1">
		<name>librarianInformation</name>
		<value>{translate key="default.pressSettings.forLibrarians"}</value>
	</setting>
	<setting type="object" locale="0">
		<name>supportedLocales</name>
		<value>
			<array>
				<element>{$primaryLocale}</element>
			</array>
		</value>
	</setting>
	<setting type="object" locale="1">
		<name>bookFileTypes</name>
		<value>
			<array>
				<element>
					<array>
						<element key="prefix">--</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.artwork"}</element>
						<element key="sortable">1</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.artworkDescription"}</element>
					</array>
				</element>
				<element>
					<array>
						<element key="prefix">99A</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.appendix"}</element>
						<element key="sortable">0</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.appendixDescription"}</element>
					</array>
				</element>
				<element>
					<array>
						<element key="prefix">99B</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.bibliography"}</element>
						<element key="sortable">0</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.bibliographyDescription"}</element>
					</array>
				</element>
				<element>
					<array>
						<element key="prefix">99Z</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.manuscript"}</element>
						<element key="sortable">0</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.manuscriptDescription"}</element>
					</array>
				</element>
					<element>
					<array>
						<element key="prefix">--</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.chapter"}</element>
						<element key="sortable">1</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.chapterDescription"}</element>
					</array>
				</element>
					<element>
					<array>
						<element key="prefix">99G</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.glossary"}</element>
						<element key="sortable">0</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.glossaryDescription"}</element>
					</array>
				</element>
				<element>
					<array>
						<element key="prefix">99I</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.index"}</element>
						<element key="sortable">0</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.indexDescription"}</element>
					</array>
				</element>
				<element>
					<array>
						<element key="prefix">00P</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.preface"}</element>
						<element key="sortable">0</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.prefaceDescription"}</element>
					</array>
				</element>
				<element>
					<array>
						<element key="prefix">88P</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.prospectus"}</element>
						<element key="sortable">0</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.prospectusDescription"}</element>
					</array>
				</element>
				<element>
					<array>
						<element key="prefix">--</element>
						<element key="type">{translate key="default.pressSettings.bookFileTypes.tablesAndFigures"}</element>
						<element key="sortable">1</element>
						<element key="description">{translate key="default.pressSettings.bookFileTypes.tablesAndFiguresDescription"}</element>
					</array>
				</element>
			</array>
		</value>
	</setting>
	<setting type="object" locale="1">
		<name>submissionChecklist</name>
		<value>
			<array>
				<element>
					<array>
						<element key="content">{translate key="default.pressSettings.checklist.notPreviouslyPublished"}</element>
						<element key="order">1</element>
					</array>
				</element>
				<element>
					<array>
						<element key="content">{translate key="default.pressSettings.checklist.fileFormat"}</element>
						<element key="order">2</element>
					</array>
				</element>
				<element>
					<array>
						<element key="content">{translate key="default.pressSettings.checklist.addressesLinked"}</element>
						<element key="order">3</element>
					</array>
				</element>
				<element>
					<array>
						<element key="content">{translate key="default.pressSettings.checklist.submissionAppearance"}</element>
						<element key="order">4</element>
					</array>
				</element>
				<element>
					<array>
						<element key="content">{translate key="default.pressSettings.checklist.bibliographicRequirements"}</element>
						<element key="order">5</element>
					</array>
				</element>
			</array>
		</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtAbstract</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtCaptureCite</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtViewMetadata</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtSupplementaryFiles</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtPrinterFriendly</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtAuthorBio</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtDefineTerms</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtAddComment</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtEmailAuthor</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>rtEmailOthers</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>allowRegReviewer</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>allowRegAuthor</name>
		<value>true</value>
	</setting>
	<setting type="bool" locale="0">
		<name>allowRegReader</name>
		<value>true</value>
	</setting>
</press_settings>
