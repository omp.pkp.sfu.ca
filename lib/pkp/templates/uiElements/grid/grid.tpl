{**
 * grid.tpl
 *
 * Copyright (c) 2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * form opening html tags
 *}
{assign var=numGridColumns value=$grid->getColumnTitles()|@count}

{if $grid->getPartialWidth()}
<div class="float-left" style="width: {$grid->getPartialWidth()};">
{/if}
<div id="{$grid->getId()}" class="grid">
		<div class="wrapper">			
			<span class="options">		
				{foreach from=$grid->getOptions() item=option}
					{if $option.titleTranslated}
						{assign var=optionTitleTranslated value=$option.titleTranslated}
					{else}
						{translate|assign:"optionTitleTranslated" key=$option.title}
					{/if}
				
					{** TODO: make different action/option types. (modal, link, javascript) *}
					{modal 		form="#editSponsorForm" 								
						   		url=$option.url 
								dialog="#`$grid->getId()`-optionDialog-`$option.slug`" 
								button="#`$grid->getId()`-optionDialogButton-`$option.slug`"
								appendTo="#table-`$grid->getId()` tbody"
					}
					<button type="button" id="{$grid->getId()}-optionDialogButton-{$option.slug}">{$optionTitleTranslated}</button>

					<div id="{$grid->getId()}-optionDialog-{$option.slug}" title="{$optionTitleTranslated}"></div>
				{/foreach}				
			</span>
			<h3>{$grid->getTitle()}</h3>
			<table id="table-{$grid->getId()}">
			    <colgroup>
			    	{"<col />"|str_repeat:$numGridColumns}
			    </colgroup>
			    <thead>
			    	<tr>
			    		{** build the columns **}
			    		{foreach from=$grid->getColumnTitles() item=column}
			        	<th scope="col">{$column|escape}</th>
						{/foreach}
			        </tr>
			    </thead>
			    <tbody>
				    {** allow for array and Iterator for rows **}
					{assign var=iterator value=$grid->getIterator()}
				    {if !$iterator || is_array($iterator)}
					    {foreach name=gridIterator from=$iterator item=rowData}
					    	{assign var=row value=$grid->buildRow($rowData)}
					    	
					    	{** default the id of the row to the row number if no id was passed in the rowData **}
					    	{if !$row->getId()} 
					    		{$row->setId("`$grid->getId()`.row.`$smarty.foreach.gridIterator.index`")}
					    	{/if}
					    	{include file="uiElements/grid/gridRow.tpl" row=$row}
						{foreachelse}
							<tr>
								<td colspan={$numGridColumns}>{translate key="no items"}</td>
							</tr>
						{/foreach}
					{else}
						{iterate from=$iterator item=object}
					    	{include file="uiElements/grid/gridRow.tpl" row=$object->buildRow()}
						{/iterate}
						{if $iterator->wasEmpty}
							<tr>
								<td colspan={$numGridColumns}>{translate key="no items"}</td>
							</tr>						
						{/if}
					{/if}
			    </tbody>
			</table>
			<div class="actions">
				{foreach from=$grid->getActions() item=action}
					<a id="actionLink">
						{if $action.titleTranslated}
							{$action.titleTranslated}
						{else}
							{translate key=$action.title}
						{/if}</a>
				{/foreach}
			</div>
		</div>
	</div>
{if $grid->getPartialWidth()}
</div>
{/if}
