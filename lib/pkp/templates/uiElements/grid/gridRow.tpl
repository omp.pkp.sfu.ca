{**
 * grid.tpl
 *
 * Copyright (c) 2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * a regular grid row
 *}

					<tr id="{$row->getId()}">
					{foreach from=$row->getCells() item=cell}
						{include file="uiElements/grid/gridCell.tpl cell=$cell}
					{/foreach}			    	
					</tr>
