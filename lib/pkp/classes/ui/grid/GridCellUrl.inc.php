<?php

/**
 * @file classes/ui/grid/GridCellUrl.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class GridCellUrl
 * @ingroup ui.grid
 *
 * @brief Class defining an HTML table cell
 */

import('ui.grid.GridCell');

class GridCellUrl extends GridCell{
	var $url;
	
	function setUrl($url) {
		$this->url = $url;
	}
	
	function getContents() {
		if ( $this->url && $this->text) return '<a href="' . $this->url . '">'.$this->text.'</a>';
		else return parent::getContents;
	}
}

?>
