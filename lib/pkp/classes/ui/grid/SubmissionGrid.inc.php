<?php

/**
 * @file classes/uiElements/grid/Grid.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class Grid
 * @ingroup core
 *
 * @brief Class defining basic operations for handling HTML grids.
 */

import('ui.grid.Grid');

class SubmissionGrid extends Grid {
	/**
	 * Constructor.
	 * @param $template string the path to the grid template file
	 */
	function SubmissionGrid() {
		parent::Grid($template);
		
		$this->options[] = array('url' => 'url2', 'title' => 'Add File');
	}
	
	function buildRow(&$submission) {
		//FIXME: getMonographId should be replaced with getId() (same in OJS and OCS)
		$rowId = 'grid' . $this->getId() . '.row' . $submission->getMonographId();
		
		$row =& new GridRow($rowId);
		
		$cellIdPrefix = $rowId . '.';
		$filenameCell =& new GridCell($cellIdPrefix . '1');			
		$filenameCell->setContents($submission->getLocalizedTitle());
		$row->addCell(&$filenameCell);			
		
		$modifiedCell =& new GridCell($cellIdPrefix . '2');
		$modifiedCell->setContents('today');
		$row->addCell(&$modifiedCell);
		return $row;
	}
}

?>
