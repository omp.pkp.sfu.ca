<?php

/**
 * @file classes/ui/grid/FileGrid.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class FileGrid
 * @ingroup ui.grid
 *
 * @brief Class defining basic operations for handling file grids.
 */

import('ui.grid.Grid');

class FileGrid extends Grid {

	var $currentRole;
	
	var $type;
	
	/** array with role names **/
	var $roles;
	
	/**
	 * Constructor.
	 * @param $template string the path to the grid template file
	 */
	function FileGrid($template = 'ui/grid/fileGrid.tpl') {
		parent::Grid($template);
	}
	
	function getRoles() {
		return $this->roles;
	}
	
	function setRoles($roles) {
		$this->roles = $roles;
	}
	
	function getColumnTitles() {
		return array_merge(array('file name', 'my role'), $this->roles);;
	}
	
	function newFile() {
		parent::addRow($row);		
	}
	
	function getGridActions() {
	
	}
	
	function sort($categoryId) {
		
	}
	
}

?>
