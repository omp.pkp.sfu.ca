<?php


/**
 * @file classes/ui/grid/GridCell.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class GridCell
 * @ingroup core
 *
 * @brief Class defining basic operations for handling HTML grids.
 */

class GridCell {
	/** Internal identifier for a given cell. To be used as a programmatic reference. */
	var $id;
	
	var $action;

	/** Reference to the row object to which this cell belongs **/
	var $row;
	
	var $content;
	/**
	 * Constructor.
	 * @param id of int
	 */
	function GridCell($id = null) {
		$this->id = $id;		
	}
	
	function setId($id) {
		$this->id = $id;
	}
	
	function getId() {
		return $this->id;
	}

	function getRow() {
		return $this->row;
	}
	
	function setRow(&$row) {
		$this->row =& $row;
	}
	
	function setContents($content) {
		$this->content = $content;
	}
	
	function getContents() {
		return $this->content;
	}
	
	function getGridCellActions() {
	}
}

?>
