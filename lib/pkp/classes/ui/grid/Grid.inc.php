<?php

/**
 * @file classes/uiElements/grid/Grid.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class Grid
 * @ingroup core
 *
 * @brief Class defining basic operations for handling HTML grids.
 */

import('ui.grid.GridRow');

class Grid {

	/** The template file containing the HTML grid */
	var $_template;

	/** Internal identifier for a given grid. To be used as a programmatic reference. */
	var $id;
	
	/** The title of the overall grid module.  **/
	var $title;
	
	/** The title of each of the columns **/
	var $columnTitles;
	
	/** The categories in which rows are divided **/
	var $categories;
	
	/** Actions that can be performed on the grid **/
	var $actions;

	/** Rows that form part of the grid (Array) **/
	var $rowData;
	
	/** name of iterator to be used **/
	var $iterator;

	/**
	 * Constructor.
	 * @param $template string the path to the grid template file
	 */
	function Grid($template = 'uiElements/grid/grid.tpl') {	
		$id = $this->id;
		// Hardcode a couple of actions for a standard grid
		$this->actions = array( array('url' => "javascript:gridAddRow('$id');", 
										'title' => 'grid.addItem') );
							
		$this->options = array();

	}
	
	/** 
	 * GET/SET grid parts 
	 */
	
	function getId() {
		return $this->id;
	}
	
	function setId($id) {
		$this->id = $id;
	}
	
	function setTitle($title) {
		$this->title = $title;
	}
	
	function getTitle() {
		return $this->title;
	}

	function setActions($actions) {
		$this->actions = $actions;
	}
	
	function addAction($action) {
		$this->actions[] = $action;
	}
	
	function getActions() {
		return $this->actions;
	}
	
	function setOptions($options) {
		$this->options = $options;
	}
	
	function getOptions() {
		return $this->options;
	}

	function addOption($option) {
		$this->options[] = $option;
	}
	
	function setPartialWidth($partialWidth) {
		$this->partialWidth = $partialWidth;
	}
	
	function getPartialWidth() {
		return $this->partialWidth;
	}
	
	function setColumnTitles($titles) {
		$this->columnTitles = $titles;
	}
	
	function getColumnTitles() {
		return $this->columnTitles;
	}
	
	function setRowData($rowData) {
		return $this->rowData = $rowData;
	}
	
	function getRowData() {
		return $this->rowData;
	}
	
	function setIterator($iterator) {
		$this->iterator = $iterator;
	}
	
	function getIterator() {
		return $this->iterator;
	}

	function sort($categoryId) {
		
	}
		
	// 
	// Method to build Row from an array.  
	// Other types of grids an override this method to build rows from Iterators
	// $rowData Array (each array element is a cell in this row)
	function buildRow(&$rowData) {
		// if array contains the key 'id' then use it for the rowId and do not place the id in a cell
		if ( isset($rowData['id']) ) 
			$rowId = 'grid' . $this->getId() . '.row' . $rowData['id'];
		else 
			$rowId = null;

		$row =& new GridRow($rowId);

		foreach ($rowData as $key => $value) {
			// TODO: why does it have to be === 0 . == 0 returns true always?
			if ( $key == 'id' || $key === 0) continue;
			$cell =& new GridCell();			
			$cell->setContents($value);
			$row->addCell($cell);
			unset($cell);			
		}

		return $row;
	}
}

?>
