<?php

/**
 * @file classes/ui/grid/GridRow.inc.php
 *
 * Copyright (c) 2000-2009 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class GridRow
 * @ingroup core
 *
 * @brief Class defining basic operations for handling HTML grids.
 */

import('ui.grid.GridCell');

class GridRow {
	/** Internal identifier for a given row. To be used as a programmatic reference. */
	var $id;
	
	/** Internal Array representation of the cells in this row **/
	var $cells;
	
	/** boolean flag for notes **/
	var $hasNotes;
	
	/** The Grid object to which this row belongs **/
	var $grid;

	/**
	 * Constructor.
	 * @param $id Internal identifier for this row
	 */
	function GridRow($id = null) {
		$this->id = $id;
		$this->cells = array();
	}
	
	function setId($id) {
		$this->id = $id;
	}
	
	function getId() {
		return $this->id;
	}
	
	function setGrid(&$grid) {
		$this->grid =& $grid;
	}
	
	function getGrid() {
		return $this->grid;
	}
	
	function getCells() {
		return $this->cells;
	}
	
	
	// 
	//  Row Actions
	//

	/**
	 * Add a cell to this row.  If cell has an ID, the make cells array associative
	 * @param $cell Cell (a RowCell object)
	 */
	function addCell(&$cell) {
		// take ownership of the cell
		$cell->setRow($this);
		if ( $cell->getId() )
			$this->cells[$cell->getId()] =& $cell;
		else
			$this->cells[] =& $cell;
	}
	
	function showInformation() {
		
	}

	function remove() {
		return $this->grid->removeRow($this->id);
	}	
}

?>
