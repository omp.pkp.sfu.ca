{**
 * acquisitionsEditor.tpl
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Acquisitions Editor navigation sidebar.
 *
 * $Id: acquisitionsEditor.tpl,v 1.1 2009/06/09 23:37:15 tylerl Exp $
 *}
<div class="block" id="sidebarAcquisitionsEditor">
	<span class="blockTitle">{translate key="user.role.acquisitionsEditor"}</span>
	<span class="blockSubtitle">{translate key="monograph.submissions"}</span>
	<ul>
		<li><a href="{url op="index" path="submissionsInReview"}">{translate key="common.queue.short.submissionsInReview"}</a>&nbsp;({if $submissionsCount[0]}{$submissionsCount[0]}{else}0{/if})</li>
		<li><a href="{url op="index" path="submissionsInEditing"}">{translate key="common.queue.short.submissionsInEditing"}</a>&nbsp;({if $submissionsCount[1]}{$submissionsCount[1]}{else}0{/if})</li>
		<li><a href="{url op="index" path="submissionsArchives"}">{translate key="common.queue.short.submissionsArchives"}</a></li>
	</ul>
</div>
 
