{**
 * block.tpl
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site sidebar menu -- "Developed By" block.
 *
 * $Id: block.tpl,v 1.1.1.1 2008/10/20 21:27:10 tylerl Exp $
 *}
<div class="block" id="sidebarDevelopedBy">
	<a class="blockTitle" href="http://pkp.sfu.ca/omp/" id="developedBy">{translate key="common.openMonographPress"}</a>
</div>	
