<?php

/**
 * @defgroup plugins_themes_classicNavy
 */
 
/**
 * @file plugins/themes/classicNavy/index.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_themes_classicNavy
 * @brief Wrapper for "classic navy" theme plugin.
 *
 */

// $Id: index.php,v 1.1.1.1 2008/10/20 21:27:09 tylerl Exp $


require_once('ClassicNavyThemePlugin.inc.php');

return new ClassicNavyThemePlugin();

?>
