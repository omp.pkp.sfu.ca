<?php 

/**
 * @defgroup plugins_generic_translator
 */
 
/**
 * @file plugins/generic/translator/index.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_generic_translator
 * @brief Wrapper for translation maintenance plugin.
 *
 */

// $Id: index.php,v 1.1.1.1 2008/10/20 21:27:09 tylerl Exp $


require_once('TranslatorPlugin.inc.php');

return new TranslatorPlugin(); 

?> 
