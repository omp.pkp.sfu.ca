<?php

/**
 * @defgroup plugins_generic_jquery
 */
 
/**
 * @file plugins/generic/jquery/index.php
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_generic_jquery
 * @brief Wrapper for jQuery plugin.
 *
 */

// $Id: index.php,v 1.1 2009/09/14 19:31:38 mcrider Exp $


require_once('JQueryPlugin.inc.php');

return new JQueryPlugin();

?> 
