/**
 * modal.js
 *
 * Copyright (c) 2003-2008 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Implementation of jQuery modals for OJS.
 *
 * $Id: 
 */

/**
 * modal
 * @param $dialog Selector (ID) for the div that will contain the modal
 * @param $url URL to load into the modal
 * @param $form Selector for the form
 * @param $appendTo Selector for container element that content will be appended to
 */
function modal(dialog, url, form, appendTo) {
	$(document).ready(function() {
		// Construct dialog			
		$(dialog).load(url).dialog({
			autoOpen: false,
			width: 600,
			modal: true,
			draggable: false,
			buttons: {
				"Ok": function() { 
					$(form).validate();		
					
					// Post to server and construct callback
					if ($(form).valid()) {
						$.post(
							$(form).attr("action"),
							$(form).serialize(),
							function(returnString) {
								if (returnString.status == true) {
									$(appendTo).append(returnString.content);
									$(dialog).dialog("close");
								} else {
									// Display errors in error list
									$('#formErrors .formErrorList').html(returnString.content);
								}
							},
							"json"
						);
					} 
				}, 
				"Cancel": function() { 
					$(this).dialog("close"); 
				} 
			}
		});
	});
}

function changeModalFormLocale() {
	oldLocale = $("#currentLocale").val();
	newLocale = $("#formLocale").val();

	$("#currentLocale").val(newLocale);
	$("."+oldLocale).hide();
	$("."+newLocale).show("normal");
}
