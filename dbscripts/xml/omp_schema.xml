<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE schema SYSTEM "../../lib/pkp/dtd/xmlSchema.dtd">

<!--
  * omp_schema.xml
  *
  * Copyright (c) 2003-2008 John Willinsky
  * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
  *
  * OMP database schema in XML.
  *
  * $Id: omp_schema.xml,v 1.31 2009/09/24 17:20:35 tylerl Exp $
  -->

<schema version="0.2">

	<!--
	  *
	  * TABLE acquisitions_arrangements (series/divisions)
	  *
	  -->
	<table name="acquisitions_arrangements">
		<field name="arrangement_id" type="I8">
			<KEY />
			<AUTOINCREMENT />
		</field>
		<field name="arrangement_type" type="I8">
			<NOTNULL/>
		</field>
		<field name="press_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="review_form_id" type="I8" />
		<field name="seq" type="F">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="editor_restricted" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="meta_indexed" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="hide_about" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="disable_comments" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<descr>Press acquisition arrangements.</descr>
		<index name="arrangement_press_id">
			<col>press_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE acquisitions_arrangements_settings
	  *
	  -->
	<table name="acquisitions_arrangements_settings">
		<field name="arrangement_id" type="I8">
			<NOTNULL />
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL />
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL />
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
			<descr>(bool|int|float|string|object)</descr>
		</field>
		<descr>Arrangement-specific settings</descr>
		<index name="aspect_settings_aspect_id">
			<col>aspect_id</col>
		</index>
		<index name="arrangement_settings_pkey">
			<col>arrangement_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE acquisitions_arrangements_editors
	  *
	  -->
	<table name="acquisitions_arrangements_editors">
		<field name="press_id" type="I8">
			<NOTNULL />
		</field>
		<field name="arrangement_id" type="I8">
			<NOTNULL />
		</field>
		<field name="user_id" type="I8">
			<NOTNULL />
		</field>
		<field name="can_edit" type="I1">
			<NOTNULL />
			<DEFAULT VALUE="1" />
		</field>
		<field name="can_review" type="I1">
			<NOTNULL />
			<DEFAULT VALUE="1" />
		</field>
		<descr>Assignments of acquisition editors to acquisitions arrangements.</descr>
		<index name="acquisitions_arrangements_editors_press_id">
			<col>press_id</col>
		</index>
		<index name="acquisitions_arrangements_editors_arrangement_id">
			<col>arrangement_id</col>
		</index>
		<index name="acquisitions_arrangements_editors_user_id">
			<col>user_id</col>
		</index>
		<index name="acquisitions_arrangements_editors_pkey">
			<col>press_id</col>
			<col>arrangement_id</col>
			<col>user_id</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE edit_assignments
	  *
	  -->
	<table name="edit_assignments">
		<field name="edit_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="editor_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="can_edit" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="1"/>
		</field>
		<field name="can_review" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="1"/>
		</field>
		<field name="date_assigned" type="T"/>
		<field name="date_notified" type="T"/>
		<field name="date_underway" type="T"/>
		<descr>Editing assignments.</descr>
		<index name="edit_assignments_monograph_id">
			<col>monograph_id</col>
		</index>
		<index name="edit_assignments_editor_id">
			<col>editor_id</col>
		</index>
	</table>
	<!--
	  *
	  * TABLE edit_decisions
	  *
	  -->
	<table name="edit_decisions">
		<field name="edit_decision_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="review_type" type="I8" />
		<field name="round" type="I1">
			<NOTNULL/>
		</field>
		<field name="editor_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="decision" type="I1">
			<NOTNULL/>
		</field>
		<field name="date_decided" type="T">
			<NOTNULL/>
		</field>
		<descr>Editor decisions.</descr>
		<index name="edit_decisions_monograph_id">
			<col>monograph_id</col>
		</index>
		<index name="edit_decisions_editor_id">
			<col>editor_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE monographs
	  *
	  -->
	<table name="monographs">
		<field name="monograph_id" type="I8">
			<KEY />
			<AUTOINCREMENT />
		</field>
		<field name="user_id" type="I8">
			<NOTNULL />
		</field>
		<field name="press_id" type="I8">
			<NOTNULL />
		</field>
		<field name="arrangement_id" type="I8" />
		<field name="edited_volume" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="language" type="C2" size="10">
			<DEFAULT VALUE="en"/>
		</field>
		<field name="comments_to_ed" type="X"/>
		<field name="date_submitted" type="T"/>
		<field name="last_modified" type="T"/>
		<field name="date_status_modified" type="T"/>
		<field name="status" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="1"/>
		</field>
		<field name="submission_progress" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="1"/>
		</field>
		<field name="submission_file_id" type="I8"/>
		<field name="revised_file_id" type="I8"/>
		<field name="review_file_id" type="I8"/>
		<field name="editor_file_id" type="I8"/>
		<field name="pages" type="C2" size="255"/>
		<field name="fast_tracked" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="hide_author" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="comments_status" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="current_review_type" type="I8"/>
		<field name="current_round" type="I8"/>
		<descr>Monographs.</descr>
		<index name="monographs_user_id">
			<col>user_id</col>
		</index>
		<index name="monographs_press_id">
			<col>press_id</col>
		</index>
		<index name="monographs_arrangement_id">
			<col>arrangement_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_settings
	  *
	  -->
	<table name="monograph_settings">
		<field name="monograph_id" type="I8">
			<NOTNULL />
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL />
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL />
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
			<descr>(bool|int|float|string|object)</descr>
		</field>
		<descr>Monograph metadata.</descr>
		<index name="monograph_settings_monograph_id">
			<col>monograph_id</col>
		</index>
		<index name="monograph_settings_pkey">
			<col>monograph_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE/>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_supplementary_files
	  *
	  -->
	<table name="monograph_supplementary_files">
		<field name="supp_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="file_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="type" type="C2" size="255"/>
		<field name="language" type="C2" size="10"/>
		<field name="public_supp_file_id" type="C2" size="255"/>
		<field name="date_created" type="D"/>
		<field name="show_reviewers" type="I1">
			<DEFAULT VALUE="0"/>
		</field>
		<field name="date_submitted" type="T">
			<NOTNULL/>
		</field>
		<field name="seq" type="F">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<descr>Supplementary files attached to monographs.</descr>
		<index name="monograph_supplementary_files_file_id">
			<col>file_id</col>
		</index>
		<index name="monograph_supplementary_files_monograph_id">
			<col>monograph_id</col>
		</index>
		<index name="supp_public_supp_file_id">
			<col>public_supp_file_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_supp_file_settings
	  *
	  -->
	<table name="monograph_supp_file_settings">
		<field name="supp_id" type="I8">
			<NOTNULL />
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL />
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL />
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
			<descr>(bool|int|float|string|object|date)</descr>
		</field>
		<descr>Monograph supplementary file metadata.</descr>
		<index name="monograph_supp_file_settings_supp_id">
			<col>supp_id</col>
		</index>
		<index name="monograph_supp_file_settings_pkey">
			<col>supp_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE/>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_comments
	  *
	  -->
	<table name="monograph_comments">
		<field name="comment_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="comment_type" type="I8"/>
		<field name="role_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="assoc_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="author_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="comment_title" type="C2" size="90">
			<NOTNULL/>
		</field>
		<field name="comments" type="X"/>
		<field name="date_posted" type="T"/>
		<field name="date_modified" type="T"/>
		<field name="viewable" type="I1" />
		<descr>Comments posted on monographs.</descr>
		<index name="monograph_comments_monograph_id">
			<col>monograph_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_galleys
	  *
	  -->
	<table name="monograph_galleys">
		<field name="galley_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="public_galley_id" type="C2" size="255" />
		<field name="locale" type="C2" size="5"/>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="file_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="assignment_id" type="I8"/>
		<field name="html_galley" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="style_file_id" type="I8"/>
		<field name="seq" type="F">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="views" type="I4">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<descr>Monograph galleys.</descr>
		<index name="monograph_galleys_monograph_id">
			<col>monograph_id</col>
		</index>
		<index name="monograph_galleys_public_id">
			<col>public_galley_id</col>
			<col>monograph_id</col>
			<UNIQUE/>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_html_galley_images
	  *
	  -->
	<table name="monograph_html_galley_images">
		<field name="galley_id" type="I8">
			<NOTNULL />
		</field>
		<field name="file_id" type="I8">
			<NOTNULL />
		</field>
		<descr>Images associated with a monograph HTML galley.</descr>
		<index name="monograph_html_galley_images_pkey">
			<col>galley_id</col>
			<col>file_id</col>
			<UNIQUE/>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_files
	  *
	  -->
	<table name="monograph_files">
		<field name="file_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="revision" type="I8">
			<KEY />
		</field>
		<field name="source_file_id" type="I8" />
		<field name="source_revision" type="I8" />
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="file_name" type="C2" size="255">
			<NOTNULL/>
		</field>
		<field name="file_type" type="C2" size="255">
			<NOTNULL/>
		</field>
		<field name="file_size" type="I8">
			<NOTNULL/>
		</field>
		<field name="original_file_name" type="C2" size="127"/>
		<field name="type" type="C2" size="40">
			<NOTNULL/>
		</field>
		<field name="viewable" type="I1" />
		<field name="sortable_by_component" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="date_uploaded" type="T">
			<NOTNULL/>
		</field>
		<field name="date_modified" type="T">
			<NOTNULL/>
		</field>
		<field name="round" type="I8" />
		<field name="review_type" type="I8" />
		<field name="assoc_id" type="I8"/>
		<descr>Files associated with monograph. Includes submission files, supplementary files, etc.</descr>
		<index name="monograph_files_monograph_id">
			<col>monograph_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_file_settings
	  *
	  -->
	<table name="monograph_file_settings">
		<field name="file_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="revision" type="I8" />
		<field name="locale" type="C2" size="5">
			<NOTNULL/>
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL/>
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
			<descr>(bool|int|float|string|object)</descr>
		</field>
		<descr>Monograph file settings.</descr>
		<index name="monograph_file_settings_file_id">
			<col>file_id</col>
		</index>
		<index name="monograph_file_settings_pkey">
			<col>file_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_authors
	  *
	  -->
	<table name="monograph_authors">
		<field name="author_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="primary_contact" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="contribution_type" type="I8">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="seq" type="F">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="first_name" type="C2" size="40">
			<NOTNULL/>
		</field>
		<field name="middle_name" type="C2" size="40"/>
		<field name="last_name" type="C2" size="90">
			<NOTNULL/>
		</field>
		<field name="affiliation" type="C2" size="255"/>
		<field name="country" type="C2" size="90"/>
		<field name="email" type="C2" size="90">
			<NOTNULL/>
		</field>
		<field name="url" type="C2" size="255"/>
		<descr>Author metadata for monographs.</descr>
		<index name="monograph_authors_monograph_id">
			<col>monograph_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_author_settings
	  *
	  -->
	<table name="monograph_author_settings">
		<field name="author_id" type="I8">
			<NOTNULL />
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL />
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL />
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
			<descr>(bool|int|float|string|object)</descr>
		</field>
		<descr>Language dependent author metadata.</descr>
		<index name="monograph_author_settings_author_id">
			<col>author_id</col>
		</index>
		<index name="monograph_author_settings_pkey">
			<col>author_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE/>
		</index>
	</table>
	
	<!--
	  *
	  * TABLE monograph_components
	  *
	  -->
	<table name="monograph_components">
		<field name="component_id" type="I8">
			<KEY />
			<AUTOINCREMENT />
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL />
		</field>
		<field name="contact_author" type="I8">
			<DEFAULT VALUE="0"/>
		</field>
		<field name="seq" type="F">
			<NOTNULL />
			<DEFAULT VALUE="0"/>
		</field>
		<index name="components_component_id">
			<col>component_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_component_settings
	  *
	  -->
	<table name="monograph_component_settings">
		<field name="component_id" type="I8">
			<NOTNULL />
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL />
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL />
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
			<descr>(bool|int|float|string|object)</descr>
		</field>
		<descr>Language dependent monograph component metadata.</descr>
		<index name="monograph_component_settings_component_id">
			<col>component_id</col>
		</index>
		<index name="monograph_component_settings_pkey">
			<col>component_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE/>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_component_authors
	  *
	  -->
	<table name="monograph_component_authors">
		<field name="author_id" type="I8">
			<NOTNULL />
		</field>
		<field name="component_id" type="I8">
			<NOTNULL />
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL />
		</field>
		<field name="seq" type="F">
			<NOTNULL />
			<DEFAULT VALUE="0"/>
		</field>
		<index name="component_authors_pkey">
			<col>author_id</col>
			<col>component_id</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	 *
	 * TABLE monograph_search_keyword_list
	 *
	 -->
	<table name="monograph_search_keyword_list">
		<field name="keyword_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="keyword_text" type="C2" size="60">
			<NOTNULL/>
		</field>
		<descr>List of all keywords.</descr>
		<index name="monograph_search_keyword_text">
			<col>keyword_text</col>
			<UNIQUE/>
		</index>
	</table>

	<!--
	 *
	 * TABLE monograph_search_objects
	 *
	 -->
	<table name="monograph_search_objects">
		<field name="object_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL />
		</field>
		<field name="type" type="I4">
			<NOTNULL />
			<descr>Type of item. E.g., abstract, fulltext, etc.</descr>
		</field>
		<field name="assoc_id" type="I8">
			<descr>Optional ID of an associated record (e.g., a file_id)</descr>
		</field>
		<descr>Indexed objects.</descr>
	</table>

	<!--
	 *
	 * TABLE monograph_search_object_keywords
	 *
	 -->
	<table name="monograph_search_object_keywords">
		<field name="object_id" type="I8">
			<NOTNULL />
		</field>
		<field name="keyword_id" type="I8">
			<NOTNULL />
		</field>
		<field name="pos" type="I4">
			<NOTNULL />
			<descr>Word position of the keyword in the object.</descr>
		</field>
		<descr>Keyword occurrences for each indexed object.</descr>
		<index name="monograph_search_object_keywords_keyword_id">
			<col>keyword_id</col>
		</index>
		<index name="monograph_search_object_keywords_pkey">
			<col>object_id</col>
			<col>pos</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_event_log
	  *
	  -->
	<table name="monograph_event_log">
		<field name="log_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="user_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="date_logged" type="T">
			<NOTNULL/>
		</field>
		<field name="ip_address" type="C2" size="15">
			<NOTNULL/>
		</field>
		<field name="log_level" type="C2" size="1"/>
		<field name="event_type" type="I8"/>
		<field name="assoc_type" type="I8"/>
		<field name="assoc_id" type="I8"/>
		<field name="message" type="X"/>
		<descr>A log of all events associated with a submission.</descr>
		<index name="monograph_event_log_monograph_id">
			<col>monograph_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE monograph_email_log
	  *
	  -->
	<table name="monograph_email_log">
		<field name="log_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="sender_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="date_sent" type="T">
			<NOTNULL/>
		</field>
		<field name="ip_address" type="C2" size="15"/>
		<field name="event_type" type="I8"/>
		<field name="assoc_type" type="I8"/>
		<field name="assoc_id" type="I8"/>
		<field name="from_address" type="C2" size="255"/>
		<field name="recipients" type="X"/>
		<field name="cc_recipients" type="X"/>
		<field name="bcc_recipients" type="X"/>
		<field name="subject" type="C2" size="255"/>
		<field name="body" type="X"/>
		<descr>A log of all emails sent out related to a submission.</descr>
		<index name="monograph_email_log_monograph_id">
			<col>monograph_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE notification_status
	  *
	  -->
	<table name="notification_status">
		<field name="press_id" type="I8">
			<NOTNULL />
		</field>
		<field name="user_id" type="I8">
			<NOTNULL />
		</field>
		<descr>User "mail notifications to my account" flags</descr>
		<index name="notification_status_pkey">
			<col>press_id</col>
			<col>user_id</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE plugin_settings
	  *
	  -->
	<table name="plugin_settings">
		<field name="plugin_name" type="C2" size="80">
			<NOTNULL />
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL />
			<DEFAULT VALUE=""/>
		</field>
		<field name="press_id" type="I8">
			<NOTNULL />
		</field>
		<field name="setting_name" type="C2" size="80">
			<NOTNULL />
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
			<descr>(bool|int|float|string|object)</descr>
		</field>
		<descr>Press settings.</descr>
	</table>

	<!--
	  *
	  * TABLE presses
	  *
	  -->
	<table name="presses">
		<field name="press_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="path" type="C2" size="32">
			<NOTNULL/>
		</field>
		<field name="seq" type="F">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="primary_locale" type="C2" size="5">
			<NOTNULL/>
		</field>
		<field name="enabled" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="1"/>
		</field>
		<descr>Presses and basic press settings.</descr>
		<index name="press_path">
			<col>path</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE press_settings
	  *
	  -->
	<table name="press_settings">
		<field name="press_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL/>
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL/>
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
			<descr>(bool|int|float|string|object)</descr>
		</field>
		<descr>Press settings.</descr>
		<index name="press_settings_press_id">
			<col>press_id</col>
		</index>
		<index name="press_settings_pkey">
			<col>press_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE production_assignments
	  *
	  -->
	<table name="production_assignments">
		<field name="assignment_id" type="I8">
			<KEY />
			<AUTOINCREMENT />
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="type" type="I8" />
		<field name="label" type="C2" size="64"/>
		<descr>Production, layout, and design assignments.</descr>
		<index name="production_assignments_assignment_id">
			<col>assignment_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE review_rounds
	  *
	  -->
	<table name="review_rounds">
		<field name="monograph_id" type="I8">
			<NOTNULL />
		</field>
		<field name="round" type="I1">
			<NOTNULL />
		</field>
		<field name="review_revision" type="I8" />
		<field name="review_type" type="I8" />
		<descr>Review rounds.</descr>
		<index name="review_rounds_monograph_id">
			<col>monograph_id</col>
		</index>
		<index name="review_rounds_pkey">
			<col>monograph_id</col>
			<col>round</col>
			<col>review_type</col>
			<UNIQUE/>
		</index>
	</table>

	<!--
	  *
	  * TABLE review_assignments
	  *
	  -->
	<table name="review_assignments">
		<field name="review_id" type="I8">
			<KEY />
			<AUTOINCREMENT/>
		</field>
		<field name="monograph_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="reviewer_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="competing_interests" type="X"/>
		<field name="recommendation" type="I1"/>
		<field name="date_assigned" type="T"/>
		<field name="date_notified" type="T"/>
		<field name="date_confirmed" type="T"/>
		<field name="date_completed" type="T"/>
		<field name="date_acknowledged" type="T"/>
		<field name="date_due" type="T"/>
		<field name="last_modified" type="T"/>
		<field name="reminder_was_automatic" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="declined" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="replaced" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="cancelled" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="0"/>
		</field>
		<field name="reviewer_file_id" type="I8"/>
		<field name="date_rated" type="T"/>
		<field name="date_reminded" type="T"/>
		<field name="quality" type="I1"/>
		<field name="review_type" type="I8"/>
		<field name="round" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="1"/>
		</field>
		<field name="review_form_id" type="I8"/>
		<descr>Reviewing assignments.</descr>
		<index name="review_assignments_monograph_id">
			<col>monograph_id</col>
		</index>
		<index name="review_assignments_reviewer_id">
			<col>reviewer_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE review_forms
	  *
	  -->
	<table name="review_forms">
		<field name="review_form_id" type="I8">
			<KEY/>
			<AUTOINCREMENT/>
		</field>
		<field name="press_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="seq" type="F" />
		<field name="is_active" type="I1" />
		<descr>Review forms.</descr>
	</table>

	<!--
	  *
	  * TABLE review_form_settings
	  *
	  -->
	<table name="review_form_settings">
		<field name="review_form_id" type="I8">
			<NOTNULL />
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL />
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL />
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
		</field>
		<descr>Review form settings</descr>
		<index name="review_form_settings_review_form_id">
			<col>review_form_id</col>
		</index>
		<index name="review_form_settings_pkey">
			<col>review_form_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE review_form_elements
	  *
	  -->
	<table name="review_form_elements">
		<field name="review_form_element_id" type="I8">
			<KEY/>
			<AUTOINCREMENT/>
		</field>
		<field name="review_form_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="seq" type="F" />
		<field name="element_type" type="I8" />
		<field name="required" type="I1" />
		<descr>Review form elements.</descr>
	</table>

	<!--
	  *
	  * TABLE review_form_element_settings
	  *
	  -->
	<table name="review_form_element_settings">
		<field name="review_form_element_id" type="I8">
			<NOTNULL />
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL />
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL />
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
		</field>
		<descr>Review form element settings</descr>
		<index name="review_form_element_settings_review_form_element_id">
			<col>review_form_element_id</col>
		</index>
		<index name="review_form_element_settings_pkey">
			<col>review_form_element_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE review_form_responses
	  *
	  -->
	<table name="review_form_responses">
		<field name="review_form_element_id" type="I8">
			<NOTNULL />
		</field>
		<field name="review_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="response_type" type="C2" size="6"/>
		<field name="response_value" type="X" />
		<descr>Review form responses.</descr>
		<index name="review_form_responses_pkey">
			<col>review_form_element_id</col>
			<col>review_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE roles
	  *
	  -->
	<table name="roles">
		<field name="press_id" type="I8">
			<NOTNULL />
		</field>
		<field name="user_id" type="I8">
			<NOTNULL />
		</field>
		<field name="role_id" type="I8">
			<NOTNULL />
		</field>
		<descr>User roles in presss.</descr>
		<index name="roles_press_id">
			<col>press_id</col>
		</index>
		<index name="roles_user_id">
			<col>user_id</col>
		</index>
		<index name="roles_role_id">
			<col>role_id</col>
		</index>
		<index name="roles_pkey">
			<col>press_id</col>
			<col>user_id</col>
			<col>role_id</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE flexible_roles
	  *
	  -->
	<table name="flexible_roles">
		<field name="flexible_role_id" type="I8">
			<KEY/>
			<AUTOINCREMENT/>
		</field>
		<field name="press_id" type="I8">
			<NOTNULL />
		</field>
		<field name="type" type="I8">
			<NOTNULL />
		</field>
		<field name="enabled" type="I1">
			<NOTNULL/>
			<DEFAULT VALUE="1"/>
		</field>
		<descr>A press's flexible roles.</descr>
		<index name="flexible_roles_flexible_role_id">
			<col>flexible_role_id</col>
		</index>
	</table>

	<!--
	  *
	  * TABLE flexible_role_settings
	  *
	  -->
	<table name="flexible_role_settings">
		<field name="flexible_role_id" type="I8">
			<NOTNULL />
		</field>
		<field name="locale" type="C2" size="5">
			<NOTNULL />
			<DEFAULT VALUE=""/>
		</field>
		<field name="setting_name" type="C2" size="255">
			<NOTNULL />
		</field>
		<field name="setting_value" type="X"/>
		<field name="setting_type" type="C2" size="6">
			<NOTNULL/>
			<descr>(bool|int|float|string|object)</descr>
		</field>
		<descr>Flexible role settings</descr>
		<index name="flexible_role_settings_flexible_role_id">
			<col>flexible_role_id</col>
		</index>
		<index name="flexible_role_settings_pkey">
			<col>flexible_role_id</col>
			<col>locale</col>
			<col>setting_name</col>
			<UNIQUE />
		</index>
	</table>

	<!--
	  *
	  * TABLE flexible_role_arrangements
	  *
	  -->
	<table name="flexible_role_arrangements">
		<field name="flexible_role_id" type="I8">
			<NOTNULL />
		</field>
		<field name="arrangement_id" type="I8">
			<NOTNULL />
		</field>
		<descr>Flexible roles arranged by workflow identifiers.</descr>
	</table>

	<table name="signoff_processes">
		<field name="process_id" type="I8">
			<KEY/>
			<AUTOINCREMENT/>
		</field>
		<field name="monograph_id" type="I8" />
		<field name="status" type="I8" />
		<field name="event_type" type="I8" />
		<field name="event_id" type="I8" />
		<field name="date_initiated" type="D" />
		<field name="date_ended" type="D" />
		<field name="date_signed" type="D" />
		<descr>A part of the workflow or, a process, that is encapsulated by a signoff.</descr>
	</table>

	<table name="signoff_entities">
		<field name="entity_type" type="I8" />
		<field name="entity_id" type="I8" />
		<field name="press_id" type="I8" />
		<field name="event_type" type="I8" />
		<field name="event_id" type="I8" />
		<field name="vote" type="I8" />
		<descr>For grouping users for signoff points.</descr>
	</table>

	<table name="workflow_signoffs">
		<field name="user_id" type="I8">
			<NOTNULL/>
		</field>
		<field name="date_signed" type="T">
			<NOTNULL/>
		</field>
		<field name="process_id" type="I8">
			<NOTNULL/>
		</field>
		<descr>Misc workflow signoffs.</descr>
	</table>

</schema>
